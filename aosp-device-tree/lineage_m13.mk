#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from m13 device
$(call inherit-product, device/samsung/m13/device.mk)

PRODUCT_DEVICE := m13
PRODUCT_NAME := lineage_m13
PRODUCT_BRAND := samsung
PRODUCT_MODEL := SM-M135FU
PRODUCT_MANUFACTURER := samsung

PRODUCT_GMS_CLIENTID_BASE := android-samsung-ss

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="m13ins-user 13 TP1A.220624.014 M135FUXXS4CWH1 release-keys"

BUILD_FINGERPRINT := samsung/m13ins/m13:13/TP1A.220624.014/M135FUXXS4CWH1:user/release-keys
