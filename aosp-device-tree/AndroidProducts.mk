#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_m13.mk

COMMON_LUNCH_CHOICES := \
    lineage_m13-user \
    lineage_m13-userdebug \
    lineage_m13-eng
